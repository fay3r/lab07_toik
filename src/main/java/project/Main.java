package project;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static final String DEST = "results/tables/simple_table.pdf";
    public static final int CELL_NUM = 10;
    public static final int SPACING = 3;
    public static final String[] TEXT =
            {"First name", "Patryk",
                    "Last Name", "Starzycki",
                    "Profession", "Student",
                    "Education", "PWSZ Tarnów",
                    "Summary", "Just a student trying to learn some code"};

    public static void main(String[] args) throws IOException,
            DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new Main().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();

        Paragraph para = new Paragraph("Resume", new Font(Font.FontFamily.TIMES_ROMAN, 18,
                Font.BOLD));
        para.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(para, SPACING);
        document.add(para);

        document.add(createTable(TEXT));
        document.close();
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static PdfPTable createTable(String[] items) {
        PdfPTable table = new PdfPTable(2);
        for (int index = 0; index < CELL_NUM; index++) {
            PdfPCell cell = new PdfPCell(new Phrase(items[index]));
            cell.setPadding(10);
            table.addCell(cell);
        }
        return table;
    }

}

